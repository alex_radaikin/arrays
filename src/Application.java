import java.util.Scanner;

/**
 * Created by home on 2/3/16.
 */
public class Application {
    public static void main(String[] args) {

        Application app = new Application();
        int[] array = app.inputArray();

        app.maxElement(array);

        }

    public int[] inputArray(){

        int num_Size;
        Scanner input = new Scanner(System.in);

        System.out.println("How many elements in to your array do you want?!");
        num_Size = input.nextInt();

        int[] array = new int[num_Size];

        for (int counter = 0; counter < num_Size; counter++){
            System.out.println("Enter the digit: " + (counter + 1));
            array[counter] = input.nextInt();
        }

        input.close();

        System.out.printf("Your array is: ");
        for (int counter = 0; counter < num_Size; counter++){
            System.out.print(array[counter] + " ");
        }
        return array;
    }
    public static void maxElement(int[] arr){
        int maxElem = arr[0];
        for (int i = 1; i < arr.length; i++){
            if(arr[i] >maxElem) {
                maxElem = arr[i];
            }
        }
        System.out.println("The Max Element of Array is: " + maxElem);
    }
    }
